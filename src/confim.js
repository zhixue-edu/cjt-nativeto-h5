function Confirm(options) {
    this.tips = options.tips || '提示';
    this.confirmButtonText = options.confirmButtonText || '确定';
    this.cancelButtonText = options.cancelButtonText || '取消';
    this.callbackOk = options.callbackOk || function () {
    };
    this.callbackCancel = options.callbackCancel || function () {
    };
}

Confirm.prototype = {
    init: function () {
        this.create();
    },
    create: function () {
        var confirmWrap = document.createElement("div");
        confirmWrap.setAttribute("style", "position: fixed;font-size:16px;top:0;left:0;width:100%;height:100%;z-index:1000000;background:rgba(0,0,0,0.7);");
        var confirmHtml = "<div style='padding: 15px;width:80%;position:absolute;top:50%;left:50%;text-align:center;border-radius:10px;background:#ffffff;transform:translate(-50%,-50%);'>" +
            "<div style='color: #666666;font-size: 16px;'>" + this.tips + "</div>" +
            "<div style='margin-top:20px;font-size: 14px;display: flex;align-items: center;justify-content: center;'>" +
            "<div id='confirm-btn-ok' style='border: 1px solid #5671f4;padding: 5px 15px;border-radius: 5px;color: #5671f4;'>" + this.confirmButtonText + "</div>" +
            "<div id='confirm-btn-cancel' style='border: 1px solid #5671f4;padding: 5px 15px;border-radius: 5px;color: #666666;margin-left: 20px;'>" + this.cancelButtonText + "</div>" +
            "</div>" +
            "</div>";
        confirmWrap.innerHTML = confirmHtml;
        document.body.appendChild(confirmWrap);
        // 点击确定按钮
        document.getElementById('confirm-btn-ok').onclick = () => {
            this.callbackOk && this.callbackOk();
            confirmWrap.remove();
        }
        // 点击取消按钮
        document.getElementById('confirm-btn-cancel').onclick = () => {
            this.callbackCancel && this.callbackCancel();
            confirmWrap.remove();
        }
    }
}
export default Confirm
