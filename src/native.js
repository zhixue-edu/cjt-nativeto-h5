import confim from "./confim.js";
import axios from "axios";
class cjtNativeToH5 {
    constructor(agreement, callback) {
        //app名称
        this.name='';
        //app版本
        this.version='';
        //方法id
        this._seq = 1;
        //在app中
        this._isInApp = false;
        //间隔定时器
        this._timer1 = null;
        //超时定时器
        this._timer2 = null;
        //协议前缀
        this.agreement = agreement || 'ischool';
        //原生监听方法
        this.callback = callback || 'onShouxinerSchemeCallback';
        //host
        this.host = 'https://bigapp.scedu.net/cjt'
    }

    /* 获取平台信息 */
    _getAppInfo() {
        let ua = navigator.userAgent;
        if(ua.indexOf('wyczc')>-1){
            let uu = ua.split('wyczc')[1].trim().split(' ')[0];
            this.name = uu.split('-')[0];
            this.version = uu.split('-')[2];
        }
        else{
            console.log('协议未在相应应用中调用')
        }
    }

    //增加协议对象
    _pfnSet(fn, callback) {
        this[fn] = callback
    }

    //获取协议对象
    _pfnGet(fn) {
        return this[fn]
    }

    //删除协议对象
    _pfnRemove(fn) {
        delete this[fn]
    }

    //调用客户端协议
    _invoke(type, action, seqNum, data) {
        let uri = `${this.agreement}://${type}/${action}?cmdSeq=${seqNum}`;
        if (data) {
            uri += "&" + this._jsonToUrlEncode(data);
        }
        window.location = uri
    }

    //传参格式化
    _jsonToUrlEncode(data) {
        let str = "";
        if (data) {
            for (let item in data) {
                if (str.length > 0) {
                    str += "&"
                }
                str += (item + '=' + encodeURIComponent(data[item]));
            }
        }
        return str
    }

    //获取唯一id
    _generateCmdSeq() {
        return this._seq++
    }

    //同步关联协议调用及回调对应关系
    _invokeForCallback(type, action, callBack, data) {
        clearInterval(this._timer1);
        clearInterval(this._timer2);
        new Promise((resolve, reject) => {
            if(this._isInApp){
                resolve();
            }else {
                this._timer1 = setInterval(() => {
                    if (window.platform_cjt) {
                        clearInterval(this._timer1);
                        clearInterval(this._timer2);
                        this._isInApp = true;
                        resolve();
                    }
                }, 50);
                this._timer2 = setTimeout(() => {
                    clearInterval(this._timer1);
                    clearInterval(this._timer2);
                    this._isInApp = false;
                    reject();
                }, 500);
            }
        }).then(() => {
            let seqNum = this._generateCmdSeq();
            this._registerCallback(seqNum, callBack);
            this._invoke(type, action, seqNum, data)
        }).catch(() => {
            callBack({
                resultCode: -1,
                resultMsg: '协议未在相应应用中调用',
            })
            console.log('协议未在相应应用中调用')
        })
    }

    //设置对应协议调用加载到对象上的方法
    _registerCallback(seq, callBack) {
        this._pfnSet(seq, callBack)
    }

    //获取对应协议调用加载到对象上的方法
    _processNativeCallback(seq, data) {
        let fn = this._pfnGet(seq);
        if (fn) {
            this._pfnRemove(seq);
            fn(data);
        }
    }

    //初始化协议
    init(Name){
        this._getAppInfo();
        let name = Name || this.agreement;
        window[`${name}`] = this;
        window[`${this.callback}`] = function (a, b) {
            try {
                window[`${name}`]._processNativeCallback(a, b)
            } catch (c) {
                console.log('协议回调错误')
                //tips('协议回调错误');
            }
        }
    }

    /* 具体协议 */
    /* 隐藏标题栏（不包含系统状态栏） */
    setTitleBarVisible(a,b) {
        b = b||function (){};
        this._invokeForCallback("function", "setTitleBarVisible", b,{
            isVisible:a.isVisible
        })
    }

    /* 隐藏标题栏（包含系统状态栏） */
    setTitleBarVisibleV2(a,b) {
        b = b||function (){};
        this._invokeForCallback("function", "setTitleBarVisibleV2",b, {
            isVisible:a.isVisible
        })
    }

    /* 新开webview */
    open(a, b) {
        b = b||function (){};
        this._invokeForCallback("function", "open", b, {
            uri: a.url,
            ignoreHistory: a.ignoreHistory === undefined?true:a.ignoreHistory,
        })
    }

    /* 关闭当前webview */
    close(a) {
        this._invokeForCallback("function", "close", a)
    }

    /* 切换tab */
    goToTab(a,b){
        b = b||function (){};
        this._invokeForCallback("function", "goToTab", b, {
            index: a.index
        })
    }
    /* 调起app */
    launchApp(a,b,c){
        c = c||function (){};
        this._invokeForCallback("function", "launchApp", c, {
            package: a,
            url: b,
        })
    }

    /* 获取经纬度 */
    getLocation(a) {
        this._invokeForCallback("function", "getLocation",a)
    }

    /* 获取SUID */
    getSUID(a) {
        this._invokeForCallback("function", "getSUID",a)
    }
    /* 获取app信息 */
    getAppInfo(a) {
        this._invokeForCallback("function", "getAppInfo",a)
    }
    /* 扫一扫 */
    scanGraphicCode(a) {
        this._invokeForCallback("function", "scanGraphicCode",a)
    }

    /* 获取ClientID */
    getClientID(a) {
        this._invokeForCallback("function", "getClientID",a)
    }

    /* 设置token */
    setToken(a,b) {
        b = b||function (){};
        this._invokeForCallback("function", "setToken",b,{
            token:a.token,
            expires:a.expires,
        })
    }

    /* 获取token */
    getToken(a) {
        this._invokeForCallback("function", "getToken",a)
    }

    /* 判断app是否登录 */
    hasToken(a) {
        this._invokeForCallback("function", "hasToken",a)
    }

    /* 获取推送PushToken */
    getPushToken(a,b) {
        b = b||function (){};
        this._invokeForCallback("function", "getPushToken",b,{
            code:a.code,
        })
    }

    /* 设置Web页是否可以直接关闭 */
    ignoreHistory(a,b) {
        b = b||function (){};
        this._invokeForCallback("function", "ignoreHistory",b,{
            ignore:a.ignore,
        })
    }

    /* 调起系统相册、拍照、录像 */
    chooseMedia(a,b) {
        b = b||function (){};
        this._invokeForCallback("function", "chooseMedia", b, {
            count:a.uploadUrl||9,
            mediaType:['image'],
            sourceType:a.uploadUrl||['album','camera'],
            sizeType:['compressed'],
            camera:a.uploadUrl||'back',
        })
    }

    /* 根据auth_code获取应用地址、授权状态、应用token */
    async getAppToken(a,b){
        //判断是否登陆
        let hasToken = ()=>{
            return new Promise((resolve, reject) => {
                this.hasToken((obj) => {
                    if (obj.resultCode == 0) {
                        return resolve(obj.hasToken)
                    } else {
                        return reject(obj.resultMsg || '协议错误')
                    }
                })
            })
        }
        //状态码
        let resultCode = 0;
        //状态信息
        let resultMsg = '操作成功';
        //应用 token
        let token = '';
        //app token
        let AppToken = '';
        //是否已授权
        let auth = false; //true 已授权或不需要授权 false需要授权且未授权
        //是否需要token
        let needToken = false;
        //应用地址
        let url = '';
        //应用名称
        let name = a.name;
        //应用code
        let auth_code = a.auth_code;
        //是否自动登陆
        let auto_login = a.auto_login;
        //是否已登陆
        let isLogin = await hasToken();
        //清空token
        let delToken =()=> {
            return new Promise((resolve, reject) => {
                this.setToken({
                    token: '',
                    expires: '',
                }, (obj) => {
                    if (obj.resultCode == 0) {
                        return resolve(true)
                    } else {
                        return reject(obj.resultMsg || '协议错误')
                    }
                })
            })
        };
        //获取appToken
        let getToken = ()=> {
            return new Promise((resolve, reject) => {
                this.getToken((obj) => {
                    if (obj.resultCode == 0) {
                        return resolve(obj.token)
                    } else {
                        return reject({
                            resultCode:-2,
                            resultMsg:(obj.resultMsg || 'getToken协议错误'),
                        })
                    }
                })
            })
        };
        //弹出登录
        let doLogin = ()=> {
            this.getToken(() => {});
        };
        //关闭webview
        let closeWebview = ()=>{
            this.close(() => {});
        }
        //获取应用 token
        let appListChangeToken = ()=>{
            let params = new URLSearchParams();
            params.append('token', AppToken);
            params.append('auth_code', auth_code);
            return new Promise((resolve, reject) =>{
                axios({
                    method:"post",
                    timeout: 10000,
                    responseType: "json",
                    withCredentials: true,
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded;charset=utf-8"
                    },
                    url:`${this.host}/Upstream/CJTSso/setAppListChangeToken`,
                    data:params
                }).then((res)=>{
                    resultCode = 0;
                    resultMsg = '操作成功';
                    if(res.data.errno===0){
                        resolve(res.data.data.data)
                    }else{
                        reject({
                            resultCode:-4,
                            resultMsg:res.data.error||'网络错误',
                        })
                    }
                }).catch((error)=>{
                    reject({
                        resultCode:-1,
                        resultMsg:'获取应用token错误，请确认是否跨域'+JSON.stringify(error),
                    })
                })
            })
        }
        //用户授权应用使用自己的信息
        let userAuthApp = ()=>{
            let params = new URLSearchParams();
            params.append('token', AppToken);
            params.append('auth_code', auth_code);
            return new Promise((resolve, reject) =>{
                axios({
                    method:"post",
                    timeout: 10000,
                    responseType: "json",
                    withCredentials: true,
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded;charset=utf-8"
                    },
                    url:`${this.host}/Upstream/CJTSso/setUserAuthApp`,
                    data:params
                }).then((res)=>{
                    if(res.data.errno===0){
                        resolve(res.data.data)
                    }else{
                        reject({
                            resultCode:-4,
                            resultMsg:res.data.error||'网络错误',
                        })
                    }
                }).catch((error)=>{
                    reject({
                        resultCode:-3,
                        resultMsg:'用户授权应用使用自己的信息错误，请确认是否跨域'+JSON.stringify(error),
                    })
                })
            })
        }

        let returnResult = ()=>{
            b({AppToken,token, auth_code, auth, url,needToken,resultCode,resultMsg})
        }

        if(isLogin){
            AppToken = await getToken().catch((e)=>{
                resultCode = e.resultCode;
                resultMsg = e.resultMsg;
                returnResult();
                throw new Error(e.resultMsg)
            });
            let relResult = await appListChangeToken().catch((e)=>{
                resultCode = e.resultCode;
                resultMsg = e.resultMsg;
                returnResult();
                throw new Error(e.resultMsg)
            });
            console.log(relResult)
            if (relResult.USERAUTH === 0) {// 需要授权
                //弹窗配置
                let confirm = new confim({
                    tips: `需授权应用"${name}"使用您的个人信息`,
                    confirmButtonText: '授权',
                    cancelButtonText: '拒绝',
                    callbackOk: () => {
                        userAuthApp().then(() => {
                            auth = true;
                            url = relResult.backpath;
                            needToken = relResult.istoken == 1;
                            token = relResult.token
                            returnResult();
                        }).catch((e)=>{
                            resultCode = e.resultCode;
                            resultMsg = e.resultMsg;
                            returnResult();
                            throw new Error(e.resultMsg)
                        })
                    },
                    callbackCancel:()=>{
                        auth = false;
                        returnResult();
                    }
                });
                confirm.init();
            } else {// 已授权或不需要授权
                auth = true;
                url = relResult.backpath;
                needToken = relResult.istoken == 1;
                token = relResult.token
                returnResult();
            }
        }else{
            if(auto_login){
                delToken().then(() => {
                    returnResult();
                    closeWebview();
                    doLogin();
                });
            }else{
                returnResult();
            }
        }
    }
}

export {cjtNativeToH5};
